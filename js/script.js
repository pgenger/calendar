console.log(d3);
var cMon = new Calendar(0);

var month = 1;
var year = 2017;


var weeksArray = cMon.monthDays(year, month - 1);

var bDays = [
    {
        "name": "David Genger",
        "date": "1 / 1 / 2017",
        "eventName": "bday"
    },
    {
        "name": "Joal Genger",
        "date": "1 / 1 / 2017",
        "eventName": "bday"
    },
    {
        "name": "Chaya Genger",
        "date": "1 / 6 / 2017",
        "eventName": "bday"
    },
    {
        "name": "Nussi Einhorn",
        "date": "1 / 7 / 2017",
        "eventName": "bday"
    }
];

weeksArray.forEach(function (week, weekIndex, weekArray) {
    week.forEach(function (day, dayIndex, dayArray) {
        var currentDay = new Date(month + "/" + day + "/" + year).getTime();
        var result = bDays.filter(function (bday) {
            var bDayDate = new Date(bday.date).getTime();
            return bDayDate == currentDay;
        });
        weeksArray[weekIndex][dayIndex] = {day:currentDay, event : result}
        console.log(weeksArray[weekIndex][dayIndex]);
    })
});
console.log(weeksArray)
var calendar = d3.select('#calendar').append('table');

weeksArray.forEach(function (data) {
    calendar
        .append('tr')
        .selectAll('td')
        .data(data)
        .enter()
        .append('td')
        .attr('class', function (d) {
            if (d != 0) {
                return "month-day"
            } else {
                return;
            }
        })
        .selectAll('span')
        .data(function(d){console.log(d.event);return d.event?d.event:null})
        .enter()
        .append('span')
        .text(function(d){
            return d.name;
        })
})
